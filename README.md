# hl-tagman

this is a little script for the window manager herbstluft to manage workspaces/tags dynamically with the help of rofi. maybe later i will add dmenu support

## Getting started

it uses rofi to pick a tag to jump to or to move the focused window to, also you can add, delete or rename tags/workspaces. right now it works with flags given on the call. i recommend to put them on keybindings if used more often

## Installation

just clone the repo to a folder of your liking,

    git clone https://gitlab.com/tbreswald/hl-tagman.git
       
the install.sh will add a symlink to your ~/.local/bin and add the folder .local/bin to your PATH. just 

	cd hl-tagman
	./install.sh

it also will change the autostart file of herbstluftwm to load the changed tags from file on reboot. 
!!!ATTENTION: this might not work in any case, for example if your autostart is heavily modified or in different location. Please inspect install.sh to figure out more details and change it manually

## Usage

 hl-tagman - herbstluftwm tag/workspace manager
		
    
 USAGE:		$NAME [OPTIONS]

 OPTIONS:

      h,-h,--help     Display this message

      v,-v,--version  Display script version

      m,-m,--move     moves the focused window to the selected tag
      
      j,-j,--jump     switches to selected tag
      
      a,-a,--add      adds a tag
      
      d,-d,--delete   deletes a selected tag
      
      r,-r,--rename   renames selected tag
      
  Without any options --help is executed.

## License

Simplified BSD

## Project status

2023-07-02 further improved install.sh - now it implements the hl-rules file and does the needed changes to the herbstluft autostart to use hl-rules ... pretty experimental, BUT at least it does a backup of autostart before the changes ... just in case

2023-04-18 improved the install.sh - now it tests for the PATH before adding its part

sometime in between... adjusted the install.sh to change the herbstluftwm autostart

2023-02-02  added rule changing to the rename part, as a requirement the rules that touch tags should be outsourced in an extra file to not confuse the tagman script so now the install needs to be adjusted

2022-11-10  implementation of install.sh

2022-11-07  just the beginning

# Have fun!
