#! /usr/bin/env bash
[ ! -d "$HOME/.local/bin" ] && mkdir -p $HOME/.local/bin  
[ -f "hl-tagman" ] && [ ! -f "$HOME/.local/bin/hl-tagman" ] && ln -s $(pwd)/hl-tagman $HOME/.local/bin/
[ -d "$HOME/.config/rofi/themes" ] && cp $(pwd)/hl-tagman.rasi $HOME/.config/rofi/themes/
if [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
	echo "path already set correctly"
else
  echo "adding \"\$HOME/.local/bin\" to your \$PATH in .bashrc"
  echo 'export PATH=$PATH:$HOME/.local/bin' >> $HOME/.bashrc
fi

if pgrep -x "herbstluftwm" > /dev/null
then
  cp $HOME/.config/herbstluftwm/autostart $HOME/.config/herbstluftwm/autostart.hl-tagman-backup
  sed -i 's|tag_names=( {1..9} )|if [ -f "$HOME/.config/herbstluftwm/hl-tags" ]; then tag_names=( $(cat $HOME/.config/herbstluftwm/hl-tags) ); else tag_names=( {1..9} ); fi|' $HOME/.config/herbstluftwm/autostart
  #echo "herbstluftwm is running"
  
  ## hl-rules implementation to go here
  [ ! -f "$HOME/.config/herbstluftwm/hl-rules" ] && touch $HOME/.config/herbstluftwm/hl-rules && {
  sed  "s*hc set tree_style '╾│ ├└╼─┐'*hc set tree_style '╾│ ├└╼─┐'\n. $HOME/.config/herbstluftwm/hl-rules*" $HOME/.config/herbstluftwm/autostart
  cat > $HOME/.config/herbstluftwm/hl-rules <<EOL
#!/usr/bin/env bash

# example rule
#hc rule class=XTerm tag=3 # move all xterms to tag 3

## brought over from herbstluftwm autostart:

EOL
  grep "rule" $HOME/.config/herbstluftwm/autostart >> $HOME/.config/herbstluftwm/hl-rules
  sed -i '/rule/d' $HOME/.config/herbstluftwm/autostart
  }
else
  echo "herbstluftwm is not running" >&2
  exit 2
fi
exit 0
